import json
import os
import re


def get_json_object(file_name: str):
    with open(file_name, 'r', encoding="utf-8") as jsonFile:
        json_data = json.loads(jsonFile.read())
    return json_data


def append_template(file_name: str, out_file):
    with open(file_name) as f:
        for line in f:
            out_file.write(line)


def convert_to_class_name(s):
    return re.sub(r'_([a-z])', lambda m: m.group(1).upper(), s.capitalize()) + "TableSeeder"


def create_seeder_head_file(table_name: str):
    class_name = convert_to_class_name(table_name)
    out_file_name = class_name+".php"

    out_file = open(out_file_name, "x")
    append_template('BasicSeederTemplateHead1.txt', out_file)
    out_file.write(class_name)
    append_template('BasicSeederTemplateHead2.txt', out_file)
    out_file.write(table_name)
    append_template('BasicSeederTemplateHead3.txt', out_file)

    return out_file


def close_seeder_file(table_name: str, files_dict):
    append_template('BasicSeederTemplateFoot.txt', files_dict[table_name])
    files_dict[table_name].close()


def remove_domains_schools_webpages():
    if os.path.exists("DomainsTableSeeder.php"):
        os.remove("DomainsTableSeeder.php")
    if os.path.exists("SchoolsTableSeeder.php"):
        os.remove("SchoolsTableSeeder.php")
    if os.path.exists("WebPagesTableSeeder.php"):
        os.remove("WebPagesTableSeeder.php")


def write_json_data(json_object, seeder_dict):
    school_id = 1
    domain_id = 1
    web_page_id = 1
    for j in json_object:
        if j['country'] != "United States":
            continue

        state_province = "null"
        if j['state-province'] is not None:
            state_province = '"' + j['state-province'] + '"'

        seeder_dict["schools"].write(
            '[ ' +
            '"id" => ' + str(school_id) + ', ' +
            '"name" => "' + (j["name"]) + '", ' +
            '"alpha_two_code" => "' + j['alpha_two_code'] + '", ' +
            '"state_province" => ' + state_province + ', ' +
            '"country" => "' + j['country'] + '", ' +
            '"created_at"  => date("Y-m-d H:i:s"), ' +
            '"updated_at"  => date("Y-m-d H:i:s") ],\n'
        )

        for domain in j["domains"]:
            seeder_dict["domains"].write(
                "[ " +
                "'id' => " + str(domain_id) + ", " +
                "'name' => '" + domain + "', " +
                "'school_id' => " + str(school_id) + ", " +
                "'created_at'  => date('Y-m-d H:i:s'), " +
                "'updated_at'  => date('Y-m-d H:i:s') ],\n"
            )
            domain_id = domain_id + 1

        for web_pages in j["domains"]:
            seeder_dict["web_pages"].write(
                "[ " +
                "'id' => " + str(web_page_id) + ", " +
                "'name' => '" + web_pages + "', " +
                "'school_id' => " + str(school_id) + ", " +
                "'created_at'  => date('Y-m-d H:i:s'), " +
                "'updated_at'  => date('Y-m-d H:i:s') ],\n"
            )
            web_page_id = web_page_id + 1

        school_id = school_id + 1


if __name__ == '__main__':
    #print(("hÀ".encode('utf-8').decode('utf-8')))
    remove_domains_schools_webpages()

    json_file_name = 'world_universities_and_domains.json'
    classes = ["schools", "web_pages", "domains"]

    json_object = get_json_object(json_file_name)

    seeder_dict = dict()
    for c in classes:
        seeder_dict[c] = create_seeder_head_file(c)

    write_json_data(json_object, seeder_dict)

    for c in classes:
        close_seeder_file(c, seeder_dict)
